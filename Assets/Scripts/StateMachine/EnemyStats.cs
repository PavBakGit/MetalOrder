using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/EnemyStats")]
public class EnemyStats : ScriptableObject
{
    // Velocidades
    public float patrolSpeed = 1f;
    public float attackSpeed = 2.5f;
    // Alcance para detecci�n de objetivos
    public float reach = 50f;
    // Tiempo que tarda en abandonar una persecuci�n
    public float timeToDisengage = 20f;
    // Rango de ataque m�nimo
    public float minAttackRange = 2f;
    // Rango de ataque
    public float attackDistance = 10f;
    // Tama�o de la esfera proyectada para determinar si ve al jugador
    public float lookSphereCastRadius = 1f;
    // �ngulo de visi�n del enemigo
    public float fieldOfView = 90f;
    // Rango de audici�n de la IA
    public float hearRange = 5f;
    // Vida m�xima de la IA
    public int maxLife = 100;
    // La vida a partir del cual buscar� cobertura
    public int lifeCoverThreshold = 30;
    // Umbral de vida a partir del cual volver� a la acci�n
    public int backToActionThreshold = 80;
    // Cantidad de vida curada en cada ciclo
    public int healingAmount = 10;
    // Frecuencia de curaci�n 
    public float healingTick = 1;
    // Rango para b�squeda de cobertura
    public float coverRange = 20f;
    // Layers para la b�squeda
    public LayerMask coverLayer;
    // Layer contra los que colisionar� el raycast para verificaci�n
    public LayerMask findCoverLayer;
}
