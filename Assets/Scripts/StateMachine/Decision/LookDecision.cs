using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/Look")]
public class LookDecision : Decision
{
    // Tag del objetivo a buscar
    public string targetTag;

    public override bool Decide(StateController controller)
    {
        return Look(controller);
    }

    /// <summary>
    /// Evalua si ve a un objetivo
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool Look(StateController controller)
    {
        RaycastHit hit;
        // Configuramos el ray conforme a la posición y orientación del ojo del controller
        Ray ray = new Ray(controller.eye.position, controller.eye.forward);

        // Debug ray para ver de forma visual el alcance de visión de la IA
        Debug.DrawRay(controller.eye.position, controller.eye.forward * controller.enemyStats.reach, Color.red);

        // Evaluamos el spherecast para detectar los objetos impactados
        if(Physics.SphereCast(ray, 
                              controller.enemyStats.lookSphereCastRadius,
                              out hit,
                              controller.enemyStats.reach))
        {
            Debug.Log(hit.transform.name);
            // Revisamos si el objeto impactado contiene el tag buscado
            if(hit.collider.CompareTag(targetTag))
            {
                Debug.Log("Objetivo avistado");
                // De ser asi, asignamos como el nuevo objetivo
                controller.target = hit.transform;
                controller.lastSpottedPosition = hit.transform.position;
                // Devolvemos true al haber localizado un objetivo
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
