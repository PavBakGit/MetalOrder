using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/BackToAction")]
public class BackToActionDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return CheckLifeToBack(controller);
    }

    /// <summary>
    /// Verificamos si la vida es lo suficiente alta como para volver a la acci�n
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckLifeToBack(StateController controller)
    {
        return (controller.enemyHealth.currentLife >= controller.enemyStats.backToActionThreshold);
    }
}
