using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/LookComplex")]
public class LookComplexDecision : Decision
{
    // Para reducir la b�squeda inicial
    public LayerMask targetLayer;
    // Para localizar al aut�ntico objetivo
    public string targetTag;

    // Para activar o desactivar el modo debug
    public bool debug;

    public override bool Decide(StateController controller)
    {
        if(debug) DebugFieldOfView(controller);
        return Look(controller);
    }

    /// <summary>
    /// Localiza al objetivo mediante overlap, c�lculo de �ngulo de visi�n y verificaci�n de oclusi�n
    /// </summary>
    /// <param name="controllre"></param>
    /// <returns></returns>
    private bool Look(StateController controller)
    {
        

        RaycastHit hit;
        // Recuperamos todos los posibles objetivos de la visi�n
        Collider[] cols = Physics.OverlapSphere(controller.transform.position,
                                                controller.enemyStats.reach,
                                                targetLayer);
        // Si el n�mero de colisiones es superior a 0 significar� que hemos detectado objetivos posibles
        if(cols.Length > 0)
        {
            foreach (Collider col in cols)
            {
                // Evaluamos si el posible objetivo se encuentra dentro del �ngulo de visi�n
                if (Vector3.Angle((col.transform.position - controller.eye.position), controller.eye.forward) < controller.enemyStats.fieldOfView / 2)
                {
                    // Verificamos que exista visi�n directa con el target
                    if(Physics.Raycast(controller.eye.position,
                                       (col.transform.position - controller.eye.position),
                                       out hit,
                                       controller.enemyStats.reach))
                    {
                        // Si hay visi�n directa
                        if(hit.collider.CompareTag(targetTag))
                        {
                            // Fijamos objetivo
                            controller.target = hit.transform;
                            controller.lastSpottedPosition = hit.transform.position;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Visualizaci�n de la IA
    /// </summary>
    /// <param name="controller"></param>
    private void DebugFieldOfView(StateController controller)
    {
        Debug.DrawRay(controller.eye.position, (Quaternion.AngleAxis(controller.enemyStats.fieldOfView / 2, controller.eye.up) * controller.eye.forward) * controller.enemyStats.reach, Color.red);
        Debug.DrawRay(controller.eye.position, (Quaternion.AngleAxis(controller.enemyStats.fieldOfView / 2, -controller.eye.up) * controller.eye.forward) * controller.enemyStats.reach, Color.red);
        Debug.DrawRay(controller.eye.position, (Quaternion.AngleAxis(controller.enemyStats.fieldOfView / 2, controller.eye.right) * controller.eye.forward) * controller.enemyStats.reach, Color.red);
        Debug.DrawRay(controller.eye.position, (Quaternion.AngleAxis(controller.enemyStats.fieldOfView / 2, -controller.eye.right) * controller.eye.forward) * controller.enemyStats.reach, Color.red);
    }
}
