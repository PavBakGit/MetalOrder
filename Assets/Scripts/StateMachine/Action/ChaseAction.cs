using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenuAttribute(menuName = "AISystem/Actions/Chase")]
public class ChaseAction : StateAction
{
    public override void Act(StateController controller)
    {
        Chase(controller);
    }

    /// <summary>
    /// Desplaza a la IA hasta la �ltima posici�n en la que vio a su objetivo
    /// </summary>
    /// <param name="controller"></param>
    private void Chase(StateController controller)
    {
        // Si hay un target fijado
        if(controller.target != null)
        {
            // Fijamos la �ltima posici�n en la que fue visto como destino
            controller.navMeshAgent.destination = controller.lastSpottedPosition;
            // Iniciamos el movimiento
            controller.navMeshAgent.isStopped = false;
            // Cambiamos su velocidad para la persecuci�n
            controller.navMeshAgent.speed = controller.enemyStats.attackSpeed;
        }

        // Si llega hasta la �ltima posici�n en la que avist� al objetivo, sin que haya saltado otro estado (ataque, etc...) significar� que ya posiblemente no esta viendo a objetivo y que lo ha perdido
        if(controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance)
        {
            controller.target = null;
        }

    }
}
