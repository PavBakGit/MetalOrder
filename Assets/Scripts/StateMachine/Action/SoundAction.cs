using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Sound")]
public class SoundAction : StateAction
{
    public override void Act(StateController controller)
    {
        FindSound(controller);
    }

    /// <summary>
    /// Se desplaza hasta el origen del sonido
    /// </summary>
    /// <param name="controller"></param>
    private void FindSound(StateController controller)
    {
        if(controller.hearedSounds.Count > 0)
        {
            // Recuperamos el �ltimo sonido agregado
            controller.currentSoundPosition = controller.hearedSounds[controller.hearedSounds.Count - 1];
            // Limpiamos los dem�s
            controller.hearedSounds.Clear();
            // Iniciamos el desplazamiento hacia el sonido
            controller.navMeshAgent.destination = controller.currentSoundPosition;
            controller.navMeshAgent.isStopped = false;
            controller.navMeshAgent.speed = controller.enemyStats.attackSpeed;
        }
    }
}
