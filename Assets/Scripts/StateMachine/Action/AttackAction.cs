using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Attack")]
public class AttackAction : StateAction
{
    public override void Act(StateController controller)
    {
        Attack(controller);
    }

    /// <summary>
    /// Si est� dentro del rango de ataque, disparar� su arma
    /// </summary>
    /// <param name="controller"></param>
    private void Attack(StateController controller)
    {
        // Si la distancia con el objetivo de desplazamiento es inferior a la distancia de ataque
        if(controller.navMeshAgent.remainingDistance <= controller.enemyStats.attackDistance)
        {
            // Disparamos el arma haciendo uso delaction
            controller.OnShoot?.Invoke(controller.eye);
        }
    }
}
