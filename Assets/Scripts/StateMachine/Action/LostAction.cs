using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Lost")]
public class LostAction : StateAction
{
    public float turnSpeed = 180f;
    public float lostDuration = 4f;

    public override void Act(StateController controller)
    {
        Lost(controller);
    }

    private void Lost(StateController controller)
    {
        if (!controller.timerCounting)
        {
            controller.timerCounting = true;
            controller.stateTimer = lostDuration;
        }

        controller.navMeshAgent.isStopped = true;
        controller.transform.Rotate(0f, turnSpeed * Time.deltaTime, 0f);
        controller.stateTimer -= Time.deltaTime;
    }
}
