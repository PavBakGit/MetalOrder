using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Heal")]
public class HealAction : StateAction
{
    public override void Act(StateController controller)
    {
        HealRecover(controller);
    }

    /// <summary>
    /// Cura gradualmente a la IA en base a sus stats
    /// </summary>
    /// <param name="controller"></param>
    private void HealRecover(StateController controller)
    {
        // Si el tiempo definido para ejecutar un nuevo tick ya transcurido
        if(Time.time > controller.enemyHealth.healingTimer && controller.enemyHealth.currentLife < controller.enemyStats.maxLife)
        {
            // Aplicamos la cura en base a las estad�sticas
            controller.enemyHealth.currentLife += controller.enemyStats.healingAmount;
            // Clampeamos el resultado, para que este no sea superior al m�ximo
            controller.enemyHealth.currentLife = Mathf.Clamp(controller.enemyHealth.currentLife, 0, controller.enemyStats.maxLife);
            // Definimos el siguiente time para el tick
            controller.enemyHealth.healingTimer = Time.time + controller.enemyStats.healingTick;
        }
    }
}
