using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMove : MonoBehaviour
{
    #region VARIABLES
    // Multiplicador de desplazamiento
    public float amount = 0.02f;
    // Desplazamiento m�ximo permitido
    public float max = 0.1f;
    // Suavizado de desplazamiento
    public float smooth = 6f;
    // Variable para guardar la posici�n inicial del arma
    private Vector3 initialPosition;
    #endregion
    #region EVENTS
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        // Recuperamos el movimiento opuesto del rat�n en ambos ejes
        float movementX = -Input.GetAxis("Mouse X") * amount;
        float movementY = -Input.GetAxis("Mouse Y") * amount;
        // Hacemos un clamp del movimiento para evitar que el desplazamiento supere los l�mites impuestos
        movementX = Mathf.Clamp(movementX, -max, max);
        movementY = Mathf.Clamp(movementY, -max, max);

        // Calculamos la posici�n final del arma dado el desplazamiento del rat�n
        Vector3 finalPosition = new Vector3(movementX, movementY, 0);
        // Realizamos un movimiento hacia el destino mediante un lerp para suavizar
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPosition + initialPosition, Time.deltaTime * smooth);
    }
    #endregion
}
