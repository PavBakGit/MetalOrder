using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShoot : MonoBehaviour
{
    #region VARIABLES
    // Da�o por cada impacto del arma
    public int damage = 10;
    // Alcance del arma
    public float range = 100f;
    // Fuerza de impacto del arma
    public float impactForce = 30f;
    // Part�culas del arma
    public ParticleSystem gunShoot;
    // Prefab de las part�culas de impacto
    public GameObject impactParticles;
    // Prefab del decal de impacto
    public GameObject impactDecal;
    // Separaci�n del decal respecto a la normal de la superficie
    public float decalSeparation = 0.01f;
    // Listado de layers impactables
    public LayerMask shootableLayer;


    public float shootDelay = 0.5f;
    public float nextTimeToShoot = 0f;

    // Tama�o del cargador
    public int magazineSize = 15;
    // Cargador actual
    public int magazine;
    // El sonido cuando no hay balas
    public AudioClip noAmmoClip;
    // Para controlar si se est� realizando una recarga actualmente
    private bool isReloading = false;
    // Rango de sonido del disparo
    public float shootSoundRange = 10f;
    
    [Header("Recoil")]
    // Retroceso actual en caso de disparar
    public float currentRecoil = 0f;
    // Velocidad a la que se recupera el arma de retroceso
    public float recoilRecover = 0.5f;
    // incremento del retroceso por disparo
    public float recoilIncrement = 0.2f;
    // Multiplicador para aumentar el efecto de la curva
    public float recoilMultiplier = 10f;
    // Desviaci�n vertical del retroceso
    public AnimationCurve recoilVertical;
    // Desviaci�n horizontal del retroceso
    public AnimationCurve recoilHorizontal;

    // Para indicar si es una IA quien controla el arma
    public bool iAHolder = false;

    // Referencia al player que maneja el arma
    private WeaponShooter weaponShooter;
    // Animator del arma
    private Animator animator;
    // Audiosource del arma
    private AudioSource audioSource;




    #endregion

    #region EVENTS

    private void Awake()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        weaponShooter = GetComponentInParent<WeaponShooter>();
    }

    private void OnEnable()
    {
        if(weaponShooter != null)
        {
            weaponShooter.OnShoot += Shoot;
            weaponShooter.OnAim += Aim;
            weaponShooter.OnReload += CallReload;
        }
    }
    private void OnDisable()
    {
        if (weaponShooter != null)
        {
            weaponShooter.OnShoot -= Shoot;
            weaponShooter.OnAim -= Aim;
            weaponShooter.OnReload -= CallReload;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        // Cargamos el arma
        Reload();
    }

    // Update is called once per frame
    void Update()
    {
        // Hacemos una reducci�n gradual del retroceso
        currentRecoil -= recoilRecover * Time.deltaTime;
        // Limitamos el recoil m�ximo entre 0 y 1
        currentRecoil = Mathf.Clamp(currentRecoil, 0f, 1f);
    }

    #endregion

    #region METHODS
    /// <summary>
    /// Realiza el c�lculo de disparo y sus efectos
    /// </summary>
    /// <param name="shootTransform"></param>
    public void Shoot(Transform shootTransform)
    {
        if (Time.time < nextTimeToShoot || (iAHolder && isReloading)) return;

        nextTimeToShoot = Time.time + shootDelay;

        // Si no disponemos de munici�n suficiente
        if(magazine <= 0)
        {
            // Reproducimos el sonido de noammo
            audioSource.PlayOneShot(noAmmoClip);
            if (iAHolder) CallReload();
            // Salimos del m�todo
            return;
        }

        // Generamos un sonido que buscar� IA en el entorno
        if(!iAHolder) GenerateSound.Generate(transform.position, shootSoundRange);

        // Reducimos el tama�o del cargador actual
        magazine--;
        // Limpiamos los efectos de part�culas del arma
        gunShoot.Clear();
        // Reproducimos nuevamente
        gunShoot.Play();
        // Sonido de disparo
        audioSource.Play();
        // Animaci�n de disparo del arma
        animator.SetTrigger("Shoot");
        // Para almacenar el resultado de si el raycast ha impactado
        bool impact = false;
        // Para almacenar la informaci�n del impacto
        RaycastHit hitInfo;

        // Realizamos el raycasr utilizando el transform recibido como par�metro
        impact = Physics.Raycast(shootTransform.position, 
                                 ApplyRecoil(shootTransform.forward, shootTransform.up, shootTransform.right), 
                                 out hitInfo, 
                                 range, 
                                 shootableLayer);

        if (impact)
        {
            GameObject impactTemp = Instantiate(impactParticles,
                                                hitInfo.point,
                                                Quaternion.LookRotation(hitInfo.normal),
                                                hitInfo.transform);
            // Destruye el efecto pasado 2 segundos
            Destroy(impactTemp, 2f);

            GameObject decalTemp = Instantiate(impactDecal,
                                               hitInfo.point + hitInfo.normal * decalSeparation,
                                               Quaternion.LookRotation(-hitInfo.normal),
                                               hitInfo.transform);
            Destroy(decalTemp, 20f);

            // Verificamos si el objeto impactado dispone del componente rigidBody
            if(hitInfo.rigidbody != null)
            {
                hitInfo.rigidbody.AddForce(shootTransform.forward * impactForce, ForceMode.Impulse);
            }

            // Verificamos si el objeto impactado implementa la interfaz ITakeDamage
            ITakeDamage damageable = hitInfo.collider.GetComponent<ITakeDamage>();
            if(damageable != null)
            {
                damageable.TakeDamage(damage, weaponShooter.transform, hitInfo.point);
            }
        }
    }

    /// <summary>
    /// Realiza el apuntado con el arma
    /// </summary>
    /// <param name="setAim"></param>
    public void Aim(bool setAim)
    {
        animator.SetBool("Aim", setAim);
    }

    /// <summary>
    /// Inicia la animaci�n de recarga
    /// </summary>
    public void CallReload()
    {
        animator.SetTrigger("Reload");
        isReloading = true;
    }

    /// <summary>
    /// Realiza la recarga del arma
    /// </summary>
    public void Reload()
    {
        magazine = magazineSize;
        isReloading = false;
    }

    

    /// <summary>
    /// Aplicamos una desviaci�n en el vector de disparo, recuperando el vector modificado en funci�n de grado de retroceso
    /// </summary>
    /// <param name="originalVector"></param>
    /// <param name="vectorUp"></param>
    /// <param name="vectorRight"></param>
    /// <returns></returns>
    private Vector3 ApplyRecoil(Vector3 originalVector, Vector3 vectorUp, Vector3 vectorRight)
    {
        // Aumentamos el retroceso 
        currentRecoil += recoilIncrement;
        // Para una gesti�n m�s sencilla, limitamos el recoil m�ximo entre 0 y 1
        currentRecoil = Mathf.Clamp(currentRecoil, 0f, 1f);

        // No podemos hacer una rotaci�n con Quaternion.Euler, puesto que la har�a a nivel global
        // hacemos las toraciones utilizando un eje de referencia

        // IMPORTANTE el operador de rotaci�n no posee la propiedad conmutativa y debe relizarse siempre quaternion * vector3
        // Aplicamos el retroceso horizontal
        originalVector = Quaternion.AngleAxis(recoilHorizontal.Evaluate(currentRecoil) * -recoilMultiplier,
                                              vectorUp) * originalVector;
        // Aplicamos el retroceso vertical
        originalVector = Quaternion.AngleAxis(recoilVertical.Evaluate(currentRecoil) * -recoilMultiplier,
                                              vectorRight) * originalVector;
        // Devolvemos el vector original ya rotado en base al retroceso actual
        return (originalVector);
    }
    #endregion
}
