using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class WeaponShooter : MonoBehaviour
{
    // Action para activar o desactivar el apuntado del arma
    public Action<bool> OnAim;
    // Acci�n para realizar la recarga
    public Action OnReload;

    // Action para realizar el disparo, indicando el origen y direcci�n mediante un transform
    public Action<Transform> OnShoot;
}
