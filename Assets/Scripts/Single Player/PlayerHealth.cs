using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour, ITakeDamage
{
    #region VARIABLES
    [Header("Life")]
    // Vida m�xima
    public float maxLife = 100f;
    // Vida actual
    private float currentLife;
    // Para controlar cuando el jugador se est� auto regenerando
    public bool selfRegen;
    // Valor de regeneraci�n por segundo de vida
    public float lifeRegen = 3f;
    // Tiempo que tardar� en empezar a regenerarse tras recibir da�o
    public float lifeRegenDelay = 5f;
    // Timestamp para inicio de regeneraci�n
    private float timeToStartRegen = 0;

    public Transform[] weakPoints;
    public float weakPointRange = 0.1f;
    public float weakPointDamageMultiplier = 5f;

    // Imagen para representar el da�o recibido
    public Image damageImage;
    // Gradiente de color de la imagen
    public Gradient damageGradient;
    // Para controlar si el jugador est� muerto
    private bool isDead = false;
    #endregion

    #region EVENTS
    // Start is called before the first frame update
    void Start()
    {
        // Inicializamos la via a su valor m�ximo
        currentLife = maxLife;
    }

    // Update is called once per frame
    void Update()
    {
        SelfRegen();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        foreach (Transform wp in weakPoints)
        {
            Gizmos.DrawWireSphere(wp.position, weakPointRange);
        }
    }

    #endregion
    #region METHODS
    /// <summary>
    /// Actualiza el color de la imagen de da�o mediante la evaluaci�n del gradiante de color
    /// </summary>
    private void UpdateColor()
    {
        damageImage.color = damageGradient.Evaluate(currentLife / maxLife);
    }

    /// <summary>
    /// Incrementa la salud del jugador gradualmente
    /// </summary>
    private void SelfRegen()
    {
        if (!selfRegen || isDead) return;

        // Si el jugador no tiene la vida al m�ximo y ha pasado el tiempo para inciiar la generaci�n de vida
        if(currentLife < maxLife && timeToStartRegen <= Time.time)
        {
            currentLife += lifeRegen * Time.deltaTime;
            currentLife = Mathf.Clamp(currentLife, 0, maxLife);
            UpdateColor();
        }
    }
    /// <summary>
    /// Evalua si se ha impactado en un punto debil
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    private bool WeakPointHit(Vector3 point)
    {
        foreach (Transform wp in weakPoints)
        {
            if (Vector3.Distance(point, wp.position) < weakPointRange)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// M�todo que aplica el da�o recibido
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="origin"></param>
    /// <param name="hitPoint"></param>
    /// <returns></returns>
    public bool TakeDamage(int damage, Transform origin, Vector3 hitPoint)
    {
        // Aplicamos el da�o teniendo en cuenta si se ha impactado en un punto d�bil
        currentLife -= WeakPointHit(hitPoint) ? damage * weakPointDamageMultiplier : damage;
        UpdateColor();

        // Si el da�o ha sido mortal
        if(currentLife <= 0)
        {
            // Muere
            Dead();
            // Abandonamos el m�todo devolvient
            return true;
        }

        // En caso de no haber muerto, actualizamos timestamp para incicio de regeneraci�n
        timeToStartRegen = Time.deltaTime + lifeRegenDelay;

        // Devolvemos false
        return false;
    }
    /// <summary>
    /// Acciones a realizar para la muerte del jugador
    /// </summary>
    private void Dead()
    {
        // Si ya se ha muerto previante, salimos sin hacer nada
        if (isDead) return;
        // Indicamos que ya ha muerto
        isDead = true;

        // Desactivamos componentes de control de jugador
        GetComponent<CharacterController>().enabled = false;
        GetComponent<FPSController>().enabled = false;
        GetComponentInChildren<WeaponMove>().enabled = false;

        // Agregamos componentes de comportamiento f�sico
        gameObject.AddComponent<CapsuleCollider>().height = 2f;
        gameObject.AddComponent<Rigidbody>().mass = 1f;


    }
    #endregion
}
