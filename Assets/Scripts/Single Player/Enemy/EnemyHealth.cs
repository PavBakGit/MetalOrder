using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour, ITakeDamage
{
    #region VARIABLES
    // Controller de la m�quina de estados
    public StateController controller;
    // Vida actual
    public int currentLife;
    // Transform del origen del da�o recibido
    public Transform takingDamageFrom;
    // Para informar que se recibe da�o de un nuevo origen
    public bool newDamageOrigin = false;

    // Array con los puntos d�biles
    public Transform[] weakPoints;
    // Rango desde el cual recibe m�s da�o
    public float weakPointRange = 0.5f;
    // Multiplicador del da�o en los puntos d�biles
    public int weakPointDamageMultiplayer = 5;

    [HideInInspector]
    public float healingTimer;
    #endregion
    #region EVENTS
    // Start is called before the first frame update
    void Start()
    {
        // Inicializamos la vida con el valor m�ximo de las estad�sticas
        currentLife = controller.enemyStats.maxLife;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (weakPoints.Length <= 0) return;
        foreach (Transform wp in weakPoints)
        {
            // Pintamos una esfera que represente el �rea del punto d�bil
            Gizmos.DrawWireSphere(wp.position, weakPointRange);
        }
    }

    #endregion
    #region METHODS
    /// <summary>
    /// Aplica el da�o recibido e indica el origen del mismo
    /// Devuelve si muere por da�o recibido
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="origin"></param>
    /// <returns></returns>
    public bool TakeDamage(int damage, Transform origin, Vector3 hitPoint)
    {
        //
        currentLife -= WeakPointHit(hitPoint)? damage * weakPointDamageMultiplayer: damage;
        //
        takingDamageFrom = origin;
        if(currentLife <= 0)
        {
            Dead();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Devuelve true si se ha recibido un impacto en un punto d�bil
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    private bool WeakPointHit(Vector3 point)
    {
        foreach (Transform wp in weakPoints)
        {
            // Si el impacto se encuentra dentro del rango de un punto d�bil
            if(Vector3.Distance(point, wp.position) < weakPointRange)
            {
                Debug.Log(gameObject.name + " punto d�bil golpeado");
                // Devolvemos true
                return true;
            }
        }
        // Si llegamos hasta el final significar� que no hemos pegado dentro de ning�n rango de punto d�bil por tanto devolvemos false
        return false;
    }

    /// <summary>
    /// Realiza las acciones para desactivar la IA al morir
    /// </summary>
    private void Dead()
    {
        // Desactivamos IA
        controller.aiActive = false;
        // Desactivamos el navmesh agent
        controller.navMeshAgent.enabled = false;

        Rigidbody rb = controller.GetComponent<Rigidbody>();
        rb.drag = 0.5f;
        rb.angularDrag = 0f;
        controller.stateText.color = Color.red;
        controller.stateText.text = "XD";
        
    }
    #endregion
}
